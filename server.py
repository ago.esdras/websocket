import asyncio
import websockets

async def bandwidth_server(websocket, path):
    while True:
        data = await websocket.recv()
        await websocket.send(data)

start_server = websockets.serve(bandwidth_server, '204.48.17.107', 5000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

