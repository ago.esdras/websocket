var btn = document.getElementById('test');

btn.addEventListener('click', function () {
    const socket = new WebSocket('ws://204.48.17.107:5000');
    let start;
    socket.onopen = function (event) {
        start = new Date();
        let data = new Uint8Array(1024 * 1024); // 1 MB data
        socket.send(data);
    };

    socket.onmessage = function (event) {
        let end = new Date();
        let timeDiff = end - start;
        console.log("Bande passante: " + (1024 / (timeDiff / 1000)) + " MB/s");
        document.getElementById("response").textContent = "Bande passante: " + (1024 / (timeDiff / 1000)) + " MB/s";

    };
})
